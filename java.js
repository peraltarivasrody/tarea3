function mostrarHoraEnSegundos() {
	const ahora = new Date();
	const segundos = ahora.getHours() * 3600 + ahora.getMinutes() * 60 + ahora.getSeconds();
	document.getElementById("segundos").innerHTML = segundos;
}

function calcularArea() {
	const base = document.getElementById("base").value;
	const altura = document.getElementById("altura").value;
	const area = (base * altura) / 2;
	document.getElementById("area").innerHTML = `El área del triángulo es: ${area}`;
}

function calcularRaiz() {
	const numero = document.getElementById("numero").value;
	if (numero % 2 === 0) {
		alert("Ingrese un número impar");
		return;
	}
	const raiz = Math.sqrt(numero);
	document.getElementById("raiz").innerHTML = `La raíz cuadrada de ${numero} es: ${raiz.toFixed(3)}`;
}

function mostrarLongitud() {
	const cadena = document.getElementById("cadena").value;
	const longitud = cadena.length;
	document.getElementById("longitud").innerHTML = `La longitud de la cadena es: ${longitud}`;
}


